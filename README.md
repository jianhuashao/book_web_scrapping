reference web: https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request

git
====
1. push cmd: git push -u origin master
2. add a file: git add xx_file
3. git commit: git commit -am "xxx commit"

THE STEP TO COMMIT:
====
1. git status: to check if anything need to changes
2. git commit -a: to make a commit
3. git push -u origin master: to sync with main
4. git diff to see any difference between local version and remote version. 
5. git log: to see the history 

tig:
====
1. can be used to console text windows. 



HOW TO USE VIM: 
=====
vim is the default editor for git commit: http://www.tldp.org/LDP/intro-linux/html/sect_06_02.html

1. moving through the tet is usually possible with the arrow keys. if not try: h, l, k, j, SHIFT-G (it has to be in non-insert mode)
2. n dd will delete n lines starting from the current cursor position
3. n dw will delete n words at the right side of the cursor. 
4. x will delete the character on which the cursor is positioned
5. :n moves to line n of the file. 
6. :w will save (write) the file
7, :q will exit the editor
8, :q! force exists
9. :w newfile will save the text to newfile. 
10. /astring will search the string in the file and position the cursor on the first match below its position. 
11. / will perform the same search again. moving the cursor to the next match. 
12. yy will copy a block of text. 
13. np will past it n times. 
14: a will append it: it moves the cursor one position to the right before switching to isnert mode. 
15. i will isnert
16. o will insert a blank line under the current cursor position and move the cursor to that line. 
17. esc key to switches back to command mode. 


HOW TO USE EMCAS: 
====
http://zoo.cs.yale.edu/classes/cs210/help/emacs.html

1. C-x C-c: open a new file
2. C-x C-s: save the current file
3. C-x C-c: exist the emcas
4. C-a: go to the begining of line
5. C-e: go the end of line
6. C-n: go to the previous line 
7. C-k: kill the current line
8. C-o: open line
9. C-f: forward-char
10. C-b: backward-char
11. C-d: delete-char
12. C-space: set a region mark
13. how to copy, paste: C-space & C-space, to highlight a line, and then M-W to copy, to paste te text, process C-y. 
14. C-/, C-_, C-x u: undo.
15. C-x 1/2/3: split windows. 
16. C-x o: select another window. 
17. C-M-v: scroll the next window. 


IF THERE ANY CHANGE:
====